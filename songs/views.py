from django.shortcuts import render
from django.http import HttpResponse
from .temp_data import movie_data
from django.http import HttpResponseRedirect
from .models import Song
from .models import Post, Comment, Category

from django.urls import reverse, reverse_lazy
from django.shortcuts import render, get_object_or_404


from .forms import PostForm, CommentForm

from django.views import generic







class SongListView(generic.ListView):
    model = Post
    template_name = 'songs/index.html' 

class SongDetailView(generic.DetailView):
    model = Post
    template_name = 'songs/detail.html'

class SongCreateView(generic.CreateView):
    model = Post
    fields = ['name', 'lyrics', 'band',]
    template_name = 'songs/create.html'
    success_url = '../'

class SongUpdateView(generic.UpdateView):
    model = Post
    fields = ['name', 'lyrics', 'band',]
    template_name = 'songs/update.html'
    success_url = '../../'

class SongDeleteView(generic.DeleteView):
    model = Post
    template_name = 'songs/delete.html'
    success_url = '../../'

def create_comment(request, post_id):
    post = get_object_or_404(Post, pk=post_id)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment_author = form.cleaned_data['author']
            comment_text = form.cleaned_data['text']
            comment = Comment(author=comment_author,
                            text=comment_text,
                            posts=post)
            comment.save()
            return HttpResponseRedirect(
                reverse('songs:detail', args=(post_id, )))
    else:
        form = CommentForm()
    context = {'form': form, 'post': post}
    return render(request, 'songs/comment.html', context)

class CategoryListView(generic.ListView):
    model = Category
    template_name = 'songs/categories.html'


class CategoryDetailView(generic.DetailView):
    model = Category
    template_name = 'songs/category.html'
# Create your views here.

# def detail_song(request, song_id):
#     song = Post.objects.get(pk=song_id)
#     song = get_object_or_404(Post, pk=song_id)
#     context = {'song': song}
#     return render(request, 'songs/detail.html', context)

# def search_songs(request):
#     context = {}
#     if request.GET.get('query', False):
#         context = {
#             "song_list": [
#                 m for m in movie_data
#                 if request.GET['query'].lower() in m['name'].lower()
#             ]
#         }
#     return render(request, 'songs/search.html', context) # modifique esta linha

# def create_song(request):
#     if request.method == 'POST':
#         form = PostForm(request.POST)
#         if form.is_valid():
#             song_name = form.cleaned_data['name']
#             song_band = form.cleaned_data['band']
#             song_lyrics = form.cleaned_data['lyrics']
#             song = Post(name=song_name,
#                           band=song_band,
#                           lyrics=song_lyrics)
#             song.save()
#             return HttpResponseRedirect(
#                 reverse('songs:detail', args=(song.id, )))
#     else:
#         form = PostForm()
#     context = {'form': form}
#     return render(request, 'songs/create.html', context)

# def list_songs(request):
#     song_list = Post.objects.all()
#     context = {'song_list': song_list}
#     return render(request, 'songs/index.html', context)

# def update_song(request, song_id):
#     song = get_object_or_404(Post, pk=song_id)

#     if request.method == "POST":
#         form = PostForm(request.POST)
#         if form.is_valid():
#             song.name = form.cleaned_data['name']
#             song.band = form.cleaned_data['band']
#             song.lyrics = form.cleaned_data['lyrics']
#             song.save()
#             return HttpResponseRedirect(
#                 reverse('songs:detail', args=(song.id, )))
#     else:
#         form = PostForm(
#             initial={
#                 'name': song.name,
#                 'band': song.band,
#                 'lyrics': song.lyrics
#             })

#     context = {'song': song, 'form': form}
#     return render(request, 'songs/update.html', context)


# def delete_song(request, song_id):
#     song = get_object_or_404(Post, pk=song_id)

#     if request.method == "POST":
#         song.delete()
#         return HttpResponseRedirect(reverse('songs:index'))

#     context = {'song': song}
#     return render(request, 'songs/delete.html', context)